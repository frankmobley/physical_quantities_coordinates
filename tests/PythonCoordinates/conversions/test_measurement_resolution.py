from PythonCoordinates.conversions.measurement_resolution import nearest_neighbor_dense_sampling
import numpy as np
import pandas as pd
import unittest
import matplotlib.pylab as plt


class TestInterpolation(unittest.TestCase):
    def test_nearest_neighbor_interpolation(self):
        #   Define the input DataFrame for the desired data
        dataset = pd.DataFrame(data=[[0, 0, 0], [1, 0, 0], [1, 1, 1], [0, 1, 0], [0, 0.5, 0], [0.5, 0, 0], [1, 0.5, 0],
                                     [0.5, 1, 0]],
                               columns=['x', 'y', 'z'])

        xx, yy, s_zz, zz = nearest_neighbor_dense_sampling(dataset, np.linspace(0, 1, 10), np.linspace(0, 1, 12))

        self.assertEqual(12, xx.shape[0])
        self.assertEqual(10, xx.shape[1])
        self.assertEqual(12, yy.shape[0])
        self.assertEqual(10, yy.shape[1])
        self.assertEqual(12, zz.shape[0])
        self.assertEqual(10, zz.shape[1])

        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

        ax.plot_surface(xx, yy, s_zz)

        plt.show()

