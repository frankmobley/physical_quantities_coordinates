from unittest import TestCase
from PythonCoordinates.measurables.physical_quantities import Temperature, Pressure, Humidity
from PythonCoordinates.conversions.Physics import Physics
from pathlib import Path
import numpy as np


class TestPhysics(TestCase):
    @staticmethod
    def temp_dew_point():
        return str(Path(__file__).parents[0]) + '/Test Data/temp-dewpt-rh.csv'

    @staticmethod
    def old_psat_ratio():
        return str(
            Path(__file__).parents[0]) + '/Test Data/oldpsatratio.csv'

    @staticmethod
    def atmosphere_absorption_test_data():
        return str(
            Path(__file__).parents[0]) + '/Test Data/absorption test data.csv'

    def test_rel_humidity(self):
        data = np.loadtxt(self.temp_dew_point(), delimiter=',')
        for line in data:
            ambient_temperature = Temperature(line[0], Temperature.Units.Fahrenheit)
            dew_point = Temperature(line[1], Temperature.Units.Fahrenheit)
            humidity = Humidity(line[2])
            self.assertAlmostEqual(
                humidity.percentage,
                Physics.rel_humidity(dew_point, ambient_temperature).percentage,
                delta=1e-2)

    def test_dew_point(self):
        data = np.loadtxt(self.temp_dew_point(), delimiter=',')
        for line in data:
                ambient_temperature = Temperature(line[0], Temperature.Units.Fahrenheit)
                dew_point = Temperature(line[1], Temperature.Units.Fahrenheit)
                humidity = Humidity(line[2])
                self.assertAlmostEqual(
                    dew_point.fahrenheit,
                    Physics.dew_point(humidity, ambient_temperature).fahrenheit,
                    delta=2e-1)

    def test_molar_concentration_water_vapor(self):
        data = np.loadtxt(self.atmosphere_absorption_test_data(),delimiter=',', skiprows=1)
        for line_index in range(0, data.shape[0]):
            frequency = float(data[line_index][0])
            temp = Temperature(float(data[line_index][1]), Temperature.Units.Kelvin)
            pressure = Pressure(float(data[line_index][2]), Pressure.Units.Kilopascals)
            humidity = Humidity(float(data[line_index][3]))
            molar_concentration = float(data[line_index][4])
            self.assertAlmostEqual(
                molar_concentration,
                Physics.molar_concentration_water_vapor(pressure, humidity, temp),
                delta=1e-10)

    def test_molar_concentration_water_vapor_b(self):
        data = np.loadtxt(self.atmosphere_absorption_test_data(), delimiter=',', skiprows=1)
        for line_index in range(0, data.shape[0]):
            frequency = float(data[line_index][0])
            temp = Temperature(float(data[line_index][1]), Temperature.Units.Kelvin)
            pressure = Pressure(float(data[line_index][2]), Pressure.Units.Kilopascals)
            humidity = Humidity(float(data[line_index][3]))
            molar_concentration = float(data[line_index][4])
            self.assertAlmostEqual(
                molar_concentration,
                Physics.molar_concentration_water_vapor(pressure, humidity, temp, Physics.build_psat_ratio(temp)),
                delta=1e-10)

    def test_build_psat_ratio_old(self):
        data = np.loadtxt(self.old_psat_ratio(), delimiter=',', skiprows=1)
        for line in data:
            t = Temperature(float(line[0]), Temperature.Units.Kelvin)
            expected = float(line[2])
            actual = Physics.build_psat_ratio_old(t)
            self.assertAlmostEqual(expected, actual, delta=1e-8)
