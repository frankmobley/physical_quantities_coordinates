import datetime
from unittest import TestCase
from PythonCoordinates.conversions.GpsTime import GPSTime


class TestGpsTime(TestCase):
    def test_standard_utc(self):
        gps_time_1 = datetime.datetime(2015, 8, 17)
        self.assertEqual(gps_time_1 + datetime.timedelta(seconds=-17), GPSTime.standard_utc(gps_time_1))

        gps_time_2 = datetime.datetime(1997, 10, 12)
        self.assertEqual(gps_time_2 + datetime.timedelta(seconds=-12), GPSTime.standard_utc(gps_time_2))

        gps_time_3 = datetime.datetime(2007, 10, 12)
        self.assertEqual(gps_time_3 + datetime.timedelta(seconds=-14), GPSTime.standard_utc(gps_time_3))

    def test_standard_gps_correction(self):
        gps_time_1 = datetime.datetime(2015, 8, 17)
        self.assertEqual(-17, GPSTime.standard_gps_correction(gps_time_1))

        gps_time_2 = datetime.datetime(1997, 10, 12)
        self.assertEqual(-12, GPSTime.standard_gps_correction(gps_time_2))

        gps_time_3 = datetime.datetime(2007, 10, 12)
        self.assertEqual(-14, GPSTime.standard_gps_correction(gps_time_3))
