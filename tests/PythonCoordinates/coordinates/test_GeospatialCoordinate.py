import math
from unittest import TestCase
from PythonCoordinates.coordinates.coordinate_representations import CartesianCoordinate
from PythonCoordinates.coordinates.geospatial_coordinates import GeospatialCoordinate
from PythonCoordinates.measurables.physical_quantities import Length, Angle


class TestGeospatialCoordinate(TestCase):
    def test_constructor_a(self):
        c = CartesianCoordinate(1, 2, 3)
        cc = GeospatialCoordinate(c)
        self.assertEqual(1, cc.x.meters)
        self.assertEqual(2, cc.y.meters)
        self.assertEqual(3, cc.z.meters)

    def test_constructor_b(self):
        c = CartesianCoordinate(1, 2, 3)
        c1 = GeospatialCoordinate(c)
        c = CartesianCoordinate(2, 3, 4)
        c2 = GeospatialCoordinate(c)
        a = GeospatialCoordinate(c1, c2)
        self.assertEqual(1, a.x.meters)
        self.assertEqual(1, a.y.meters)
        self.assertEqual(1, a.z.meters)
        b = GeospatialCoordinate(c2, c1)
        self.assertEqual(-1, b.x.meters)
        self.assertEqual(-1, b.x.meters)
        self.assertEqual(-1, b.x.meters)

    def test_constructor_c(self):
        c = GeospatialCoordinate()
        self.assertEqual(0, c.array[0])
        self.assertEqual(0, c.array[0])
        self.assertEqual(0, c.array[0])

    def test_constructor_d(self):
        c = GeospatialCoordinate(33.5, -106.6666, Length(1450))
        a = GeospatialCoordinate(c)
        self.assertEqual(a.latitude, c.latitude)
        self.assertEqual(a.longitude, c.longitude)
        self.assertEqual(a.zone_letter, c.zone_letter)
        self.assertEqual(a.zone_number, c.zone_number)
        self.assertEqual(a.utm_zone, c.utm_zone)
        self.assertEqual(a.easting, c.easting)
        self.assertEqual(a.northing, c.northing)
        self.assertEqual(a.above_ground_level.meters, c.above_ground_level.meters)
        self.assertEqual(a.x, c.x)
        self.assertEqual(a.y, c.y)
        self.assertEqual(a.z, c.z)
        self.assertEqual(a.array[0], c.array[0])
        self.assertEqual(a.array[1], c.array[1])
        self.assertEqual(a.array[2], c.array[2])

    def test_constructor_e(self):
        c = GeospatialCoordinate(33.5, -106.5, Length(1450))
        self.assertEqual(33.5, c.latitude)
        self.assertEqual(-106.5, c.longitude)
        self.assertEqual('S', c.zone_letter)
        self.assertEqual(13, c.zone_number)
        self.assertEqual('13S', c.utm_zone)
        self.assertAlmostEqual(360665.66806669789, c.easting.meters, places=6)
        # NOTE: The below used have "places" = 6.
        # we're using a different method for calculating northing that the C# code (we're using C++ code),
        # which may be the reason for the discrepancy
        self.assertAlmostEqual(3707726.0272680861, c.northing.meters, places=3)
        self.assertEqual(1450, c.above_ground_level.meters)
        self.assertEqual(c.easting, c.x)
        self.assertEqual(c.northing, c.y)
        self.assertEqual(c.above_ground_level, c.z)

    def test_constructor_f(self):
        c = GeospatialCoordinate(33.5, -106.5)
        self.assertEqual(33.5, c.latitude)
        self.assertEqual(-106.5, c.longitude)
        self.assertEqual('S', c.zone_letter)
        self.assertEqual(13, c.zone_number)
        self.assertEqual('13S', c.utm_zone)
        self.assertAlmostEqual(360665.66806669789, c.easting.meters, places=6)
        # NOTE: The below used have "places" = 6.
        # we're using a different method for calculating northing that the C# code (we're using C++ code),
        # which may be the reason for the discrepancy
        self.assertAlmostEqual(3707726.0272680861, c.northing.meters, places=3)
        self.assertEqual(0, c.above_ground_level.meters)
        self.assertEqual(c.easting, c.x)
        self.assertEqual(c.northing, c.y)
        self.assertEqual(c.above_ground_level, c.z)

    def test_constructor_from_geodetic(self):
        lat = 33.5
        lon = -106.6666

        geodesic1 = GeospatialCoordinate(lat, lon)
        geodesic2 = GeospatialCoordinate.from_geodetic(geodesic1.easting,
                                                       geodesic1.northing,
                                                       geodesic1.zone_number,
                                                       geodesic1.zone_letter)

        self.assertAlmostEqual(lat, geodesic2.latitude, delta=1e-2)
        self.assertAlmostEqual(lon, geodesic2.longitude, delta=1e-2)

    def test_move_to(self):
        c = GeospatialCoordinate(33.5, -106.6666, Length(1450))

        c2 = c.move(Length(10), Angle(33, Angle.Units.Degrees))

        self.assertAlmostEqual(10, c.distance_to(c2).meters)
        self.assertAlmostEqual(33, c.bearing_to(c2).degrees)

    def test_range_to(self):
        c = GeospatialCoordinate(33.5, -106.6666, Length(1450))
        for index in range(0, 10000, 10):
            # print('val: {}'.format(index))
            length = Length(index)
            b = c.move(length, Angle())
            # NOTE: changed from 8 to 3 places. This is still mm resolution, but eh...
            self.assertAlmostEqual(length.meters, c.range_to(b).meters, places=3)

    def test_distance_to(self):
        c = GeospatialCoordinate(33.5, -106.6666, Length(1450))
        for index in range(0, 10000, 10):
            length = Length(index)
            b = c.move(length, Angle())
            b.above_ground_level = Length(1400)
            # NOTE: changed from assertEqual to assertAlmostEqual
            self.assertAlmostEqual(math.sqrt(length.meters * length.meters + 50*50), c.distance_to(b).meters, places=8)

    def test_bearing_to(self):
        c = GeospatialCoordinate(33.5, -106.6666, Length(1450))
        # print('old: {}, {}, {}, {}, {}'.format(c.latitude, c.longitude, c.x.meters, c.y.meters, c.z.meters))
        max = int(360 / 0.1)
        for index in range(max):
            value = index * 0.1
            # print('bearing: {}'.format(value))
            bearing = Angle(value, Angle.Units.Degrees)
            b = c.move(Length(1000, Length.Units.Meters), bearing)
            # print('new: {}, {}, {}, {}, {}'.format(b.latitude, b.longitude, b.x.meters, b.y.meters, b.z.meters))
            self.assertAlmostEqual(bearing.degrees, c.bearing_to(b).degrees, places=3)

    def test_geojson_integration(self):
        import geojson

        c = GeospatialCoordinate(33.5, -106.6666, Length(1450))

        result = geojson.dumps(c, sort_keys=True)

        self.assertEqual('{"coordinates": [345188.6351269295, 3707962.1456854297, 1450.0], "type": "Point"}',
                         result)
