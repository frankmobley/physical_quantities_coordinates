from unittest import TestCase
from PythonCoordinates.coordinates.coordinate_representations import CartesianCoordinate, SphericalCoordinate
from PythonCoordinates.measurables.physical_quantities import Length, Angle


class TestSphericalCoordinate(TestCase):
    def test_spherical_coordinate_a(self):
        s = SphericalCoordinate()
        self.assertEqual(0, s.r.meters)
        self.assertEqual(0, s.azimuthal.degrees)
        self.assertEqual(0, s.polar.degrees)

    def test_spherical_coordinate_b(self):
        s = SphericalCoordinate(Length(100), Angle(180), Angle(90))
        self.assertEqual(100, s.r.meters)
        self.assertEqual(90, s.azimuthal.degrees)
        self.assertEqual(180, s.polar.degrees)

    def test_spherical_coordinate_c(self):
        c = CartesianCoordinate(1, 2, 3)
        s = SphericalCoordinate(c)
        self.assertEqual(s.r.meters, c.length.meters)
        self.assertEqual(s.polar, Angle.acos(c.z / c.length))
        self.assertEqual(s.azimuthal, Angle.atan2(c.y.meters, c.x.meters))

    def test_spherical_coordinate(self):
        c = CartesianCoordinate(1, 2, 3)
        s = SphericalCoordinate(c)
        s2 = SphericalCoordinate(s)
        self.assertEqual(s.r.meters, s2.r.meters)
        self.assertEqual(s.polar, s2.polar)
        self.assertEqual(s.azimuthal, s2.azimuthal)

    def test_to_cartesian(self):
        c = CartesianCoordinate(1, 2, 3)
        s = SphericalCoordinate(c)
        c2 = s.to_cartesian()
        self.assertAlmostEqual(c.array[0], c2.array[0], places=8)
        self.assertAlmostEqual(c.array[1], c2.array[1], places=8)
        self.assertAlmostEqual(c.array[2], c2.array[2], places=8)
