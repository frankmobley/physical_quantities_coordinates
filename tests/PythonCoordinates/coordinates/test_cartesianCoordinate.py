import math
import numpy as np
from unittest import TestCase
from PythonCoordinates.coordinates.coordinate_representations import CartesianCoordinate, CylindricalCoordinate, SphericalCoordinate
from PythonCoordinates.coordinates.geospatial_coordinates import GeospatialCoordinate
from PythonCoordinates.measurables.physical_quantities import Length, Angle, Speed


class TestCartesianCoordinate(TestCase):
    def test_difference(self):
        c1 = CartesianCoordinate(Length(1, Length.Units.Meters),
                                 Length(2, Length.Units.Meters),
                                 Length(3, Length.Units.Meters))
        c2 = CartesianCoordinate(Length(2, Length.Units.Meters),
                                 Length(3, Length.Units.Meters),
                                 Length(4, Length.Units.Meters))
        c3 = c2 - c1
        self.assertEqual(1.0, c3.x.meters)
        self.assertEqual(1.0, c3.y.meters)
        self.assertEqual(1.0, c3.z.meters)
        c1 -= c1
        self.assertEqual(0, c1.x.meters)

    def test_cartesian_coordinate_a(self):
        c = CartesianCoordinate()
        self.assertEqual(0, c.array[0])
        self.assertEqual(0, c.array[1])
        self.assertEqual(0, c.array[2])
        self.assertEqual(0, c.length.meters)

    def test_cartesian_coordinate_b(self):
        c = CartesianCoordinate(Length(), Length(), Length())
        self.assertEqual(0, c.length.meters)
        c = CartesianCoordinate(Length(1, Length.Units.Meters), Length(2, Length.Units.Meters),
                                Length(3, Length.Units.Meters))
        self.assertAlmostEqual(math.sqrt(1 + 4 + 9), c.length.meters, places=14)
        self.assertEqual(1, c.array[0])
        self.assertEqual(2, c.array[1])
        self.assertEqual(3, c.array[2])
        self.assertEqual(1, c.x.meters)
        self.assertEqual(2, c.y.meters)
        self.assertEqual(3, c.z.meters)

    def test_to_spherical(self):
        c = CartesianCoordinate(Length(1, Length.Units.Meters), Length(2, Length.Units.Meters),
                                Length(3, Length.Units.Meters))
        s = SphericalCoordinate(c)
        c2 = s.to_cartesian()
        self.assertAlmostEqual(c.array[0], c2.array[0], places=10)
        self.assertAlmostEqual(c.array[1], c2.array[1], places=10)
        self.assertAlmostEqual(c.array[2], c2.array[2], places=10)

    def test_to_cylindrical(self):
        c = CartesianCoordinate(Length(1, Length.Units.Meters), Length(2, Length.Units.Meters),
                                Length(3, Length.Units.Meters))
        s = CylindricalCoordinate(c)
        c2 = s.to_cartesian()
        self.assertAlmostEqual(c.array[0], c2.array[0], places=15)
        self.assertAlmostEqual(c.array[1], c2.array[1], places=15)
        self.assertAlmostEqual(c.array[2], c2.array[2], places=15)

    def test_to_body_reference(self):
        c1 = CartesianCoordinate(Length(1, Length.Units.Meters), Length(2, Length.Units.Meters),
                                 Length(3, Length.Units.Meters))
        body = c1.to_body_reference()
        self.assertEqual(c1.x, body.y)
        self.assertEqual(c1.y, body.x)
        self.assertEqual(c1.z, -body.z)

    def test_to_geo_reference(self):
        c1 = CartesianCoordinate(Length(1, Length.Units.Meters), Length(2, Length.Units.Meters),
                                 Length(3, Length.Units.Meters))
        body = c1.to_body_reference()
        self.assertEqual(c1.x, body.y)
        self.assertEqual(c1.y, body.x)
        self.assertEqual(c1.z, -body.z)

    def test_orient_vector(self):
        c1 = CartesianCoordinate(-100, 250, 90)
        c2 = CartesianCoordinate(0, 0, 4)

        c = (c2 - c1).to_body_reference()

        c1 = CartesianCoordinate(c)
        c1.orient_vector(Angle(), Angle(), Angle())
        self.assertEqual(c.array[0], c1.array[0])
        self.assertEqual(c.array[1], c1.array[1])
        self.assertEqual(c.array[2], c1.array[2])
        c2 = CartesianCoordinate(c)
        c2.orient_vector(Angle(180, Angle.Units.Degrees),
                         Angle(45, Angle.Units.Degrees),
                         Angle(np.pi / 10.0, Angle.Units.Radians))
        self.assertAlmostEqual(c1.length.meters, c2.length.meters, places=10)
        self.assertAlmostEqual(115.9655121145938, c2.x.meters, places=2)
        self.assertAlmostEqual(-21.686959522113398, c2.y.meters, places=2)
        self.assertAlmostEqual(256.8611994573844, c2.z.meters, places=2)

    def test_nominal_wind_speed(self):
        source = GeospatialCoordinate.array_center()
        distance = Length(10, Length.Units.Meters)
        wind_speed = Speed(10, Speed.Units.Knots)
        wind_direction = Angle()
        directions = [0, 45, 90, 135, 180, 225, 270, 315]
        magnitude = [10, 7.1, 0, -7.1, -10, -7.1, 0, 7.1]
        for index in range(len(directions)):
            angle = Angle(directions[index], Angle.Units.Degrees)
            receiver = source.move(distance, angle)
            self.assertAlmostEqual(directions[index], source.bearing_to(receiver).degrees, places=0)
            temp_wind_speed = source.nominal_wind_speed(receiver_location=receiver,
                                                        velocity=wind_speed,
                                                        direction=wind_direction)
            self.assertAlmostEqual(magnitude[index], temp_wind_speed.knots, places=1)
