from unittest import TestCase
from PythonCoordinates.coordinates.coordinate_representations import CartesianCoordinate
from PythonCoordinates.coordinates.geospatial_coordinates import SourcePoint
from PythonCoordinates.measurables.physical_quantities import Length, Angle, Speed


class TestSourcePoint(TestCase):
    def test_source_point_a(self):
        sp = SourcePoint()
        self.assertEqual(0.0, sp.latitude)
        self.assertEqual(0.0, sp.longitude)
        self.assertEqual(0.0, sp.easting.meters)
        self.assertEqual(0.0, sp.northing.meters)
        self.assertEqual(0.0, sp.roll.degrees)
        self.assertEqual(0.0, sp.pitch.degrees)
        self.assertEqual(0.0, sp.heading.degrees)
        self.assertEqual(0.0, sp.ground_velocity_knots)

    def test_source_point_b(self):
        sp = SourcePoint(33.4297164720, -106.6823781916, Length(),Angle(51.9603),
                         Angle(3.2901), Angle(0.4419), Speed(186.4246, Speed.Units.MetersPerSecond))
        self.assertEqual(33.4297164720, sp.latitude)
        self.assertEqual(-106.6823781916, sp.longitude)
        self.assertAlmostEqual(3700192.248104870, sp.northing.meters, delta=1e-2)
        self.assertAlmostEqual(343596.59927807600, sp.easting.meters, delta=1e-2)
        self.assertAlmostEqual(0.4419, sp.roll.degrees, delta=1e-2)
        self.assertAlmostEqual(3.2901, sp.pitch.degrees, delta=1e-2)
        self.assertAlmostEqual(51.9603, sp.heading.degrees, delta=1e-2)
        self.assertAlmostEqual(186.4246, sp.ground_velocity.meters_per_second, delta=1e-2)

    def test_geojson_interface(self):
        import geojson

        sp = SourcePoint(
            33.4297164720, -106.6823781916, Length(), Angle(51.9603),
            Angle(3.2901), Angle(0.4419), Speed(186.4246, Speed.Units.MetersPerSecond)
            )

        result = geojson.dumps(sp, sort_keys=True)

        self.assertEqual('{"coordinates": [343596.5992773392, 3700192.2480438594, 0.0], "properties": {"heading": 51.9603, "pitch": 3.2901, "roll": 0.4419}, "type": "Point"}', result)
