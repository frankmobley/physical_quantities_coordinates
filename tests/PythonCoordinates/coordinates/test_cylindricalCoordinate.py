import math
from unittest import TestCase
from PythonCoordinates.coordinates.coordinate_representations import CartesianCoordinate, SphericalCoordinate, CylindricalCoordinate
from PythonCoordinates.measurables.physical_quantities import Length, Angle


class TestCylindricalCoordinate(TestCase):
    def test_to_cartesian(self):
        c = CartesianCoordinate(100, 200, 300)
        cyl = CylindricalCoordinate(c)
        c2 = cyl.to_cartesian()
        self.assertAlmostEqual(c.array[0], c2.array[0], places=10)
        self.assertAlmostEqual(c.array[1], c2.array[1], places=10)
        self.assertAlmostEqual(c.array[2], c2.array[2], places=10)

    def test_to_spherical(self):
        c = CartesianCoordinate(100, 200, 300)
        cyl = CylindricalCoordinate(c)
        sph = SphericalCoordinate(c)
        sph2 = cyl.to_spherical()
        self.assertEqual(sph.r.meters, sph2.r.meters)
        self.assertEqual(sph.azimuthal.degrees, sph2.azimuthal.degrees)
        self.assertEqual(sph.polar.degrees, sph2.polar.degrees)

    def test_default_constructor(self):
        cyl = CylindricalCoordinate()
        self.assertEqual(0.0, cyl.rho.meters)
        self.assertEqual(0.0, cyl.height.meters)
        self.assertEqual(0.0, cyl.phi.degrees)

    def test_generic_constructor_a(self):
        rho = Length(130, Length.Units.Feet)
        height = Length(34.5, Length.Units.Meters)
        phi = Angle(90)
        cyl = CylindricalCoordinate(rho=rho, phi=phi, height=height)
        self.assertEqual(rho.meters, cyl.rho.meters)
        self.assertEqual(height.meters, cyl.height.meters)
        self.assertEqual(phi.degrees, cyl.phi.degrees)

    def test_generic_constructor_b(self):
        c = CartesianCoordinate(100, 200, 300)
        cyl = CylindricalCoordinate(c)
        self.assertEqual(math.sqrt(10000 + 40000), cyl.rho.meters)
        self.assertEqual(c.z.meters, cyl.height.meters)
        self.assertEqual(math.atan2(c.y.meters, c.x.meters), cyl.phi.radians)
