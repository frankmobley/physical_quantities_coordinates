ECHO OFF
REM This file demonstrates the methods required to evaluate the coverage of the tests for the provided
REM python files.
REM
REM
REM Delete any pre-existing coverage analyses that have been collected
REM
coverage erase
REM
REM Coverage calculation is conducted by running the various tests included in this folder
REM
coverage run -m unittest test_MatrixRotations.py 

coverage report --omit=./*_test.py,__init__.py --include=./*.py
