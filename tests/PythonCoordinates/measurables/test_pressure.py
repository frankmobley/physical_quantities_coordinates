import unittest
from unittest import TestCase
from PythonCoordinates.measurables.physical_quantities import Pressure
from PythonCoordinates.measurables.Measurable import InvalidUnitOfMeasureException


class TestPressure(TestCase):
    def test_constructor_default(self):
        p = Pressure()
        self.assertEqual(0.0, p.kilopascal)

    def test_constructor_value(self):
        p = Pressure(99)
        self.assertEqual(float(99), p.kilopascal)

    def test_constructor_value_units(self):
        p = Pressure(10, Pressure.Units.Kilopascals)
        self.assertEqual(float(10), p.kilopascal)
        self.assertEqual(float(10000), p.pascal)
        self.assertEqual(float(0.1), p.bar)
        self.assertEqual(float(100), p.millibar)
        self.assertAlmostEqual(float(75.00615613), p.millimeter_mercury, places=8)
        self.assertAlmostEqual(float((760/(101.325 * 25.4)) * 10), p.inches_mercury, places=8)

    def test_constructor_text_units(self):
        p = Pressure('0.07', Pressure.Units.Kilopascals)
        self.assertAlmostEqual(float(0.07), p.kilopascal, places=8)

        p = Pressure('0.07', Pressure.Units.Pascal)
        self.assertAlmostEqual(float(0.07), p.pascal, places=8)

        p = Pressure('0.07', Pressure.Units.Bar)
        self.assertAlmostEqual(float(0.07), p.bar, places=8)

        p = Pressure('0.07', Pressure.Units.Millibar)
        self.assertAlmostEqual(float(0.07), p.millibar, places=8)

        p = Pressure('0.07', Pressure.Units.MillimeterMercury)
        self.assertAlmostEqual(float(0.07), p.millimeter_mercury, places=8)

        p = Pressure('0.07', Pressure.Units.InchesMercury)
        self.assertAlmostEqual(float(0.07), p.inches_mercury, places=8)

    def test_constructor_value_text(self):
        p = Pressure(-14, str(Pressure.Units.Kilopascals))
        self.assertAlmostEqual(float(-14), p.kilopascal, places=8)

        p = Pressure(-14, str(Pressure.Units.Pascal))
        self.assertAlmostEqual(float(-14), p.pascal, places=8)

        p = Pressure(-14, str(Pressure.Units.Bar))
        self.assertAlmostEqual(float(-14), p.bar, places=8)

        p = Pressure(-14, str(Pressure.Units.Millibar))
        self.assertAlmostEqual(float(-14), p.millibar, places=8)

        p = Pressure(-14, str(Pressure.Units.MillimeterMercury))
        self.assertAlmostEqual(float(-14), p.millimeter_mercury, places=8)

        p = Pressure(-14, str(Pressure.Units.InchesMercury))
        self.assertAlmostEqual(float(-14), p.inches_mercury, places=8)

    def test_constructor_text_text(self):
        p = Pressure('5.6', str(Pressure.Units.Kilopascals))
        self.assertAlmostEqual(float('5.6'), p.kilopascal, places=8)

        p = Pressure('5.6', str(Pressure.Units.Pascal))
        self.assertAlmostEqual(float('5.6'), p.pascal, places=8)

        p = Pressure('5.6', str(Pressure.Units.Bar))
        self.assertAlmostEqual(float('5.6'), p.bar, places=8)

        p = Pressure('5.6', str(Pressure.Units.Millibar))
        self.assertAlmostEqual(float('5.6'), p.millibar, places=8)

        p = Pressure('5.6', str(Pressure.Units.MillimeterMercury))
        self.assertAlmostEqual(float('5.6'), p.millimeter_mercury, places=8)

        p = Pressure('5.6', str(Pressure.Units.InchesMercury))
        self.assertAlmostEqual(float('5.6'), p.inches_mercury, places=8)

    def test_get_hash_code(self):
        v = 250
        a = Pressure(250)
        self.assertEqual(hash(v), hash(a))

    def test_copy(self):
        a = Pressure(-10)
        b = a.copy()
        self.assertTrue(a is not b)

    def test_in(self):
        x = 0.26529
        p = Pressure(x)
        self.assertEqual(p.inches_mercury, p.in_units(Pressure.Units.InchesMercury))
        self.assertEqual(p.millimeter_mercury, p.in_units(Pressure.Units.MillimeterMercury))
        self.assertEqual(p.kilopascal, p.in_units(Pressure.Units.Kilopascals))
        self.assertEqual(p.pascal, p.in_units(Pressure.Units.Pascal))
        self.assertEqual(p.bar, p.in_units(Pressure.Units.Bar))
        self.assertEqual(p.millibar, p.in_units(Pressure.Units.Millibar))

    def test_symbol_to_enum(self):
        self.assertEqual(Pressure.Units.InchesMercury, Pressure.symbol_to_enum(str(Pressure.Units.InchesMercury)))
        self.assertEqual(Pressure.Units.MillimeterMercury, Pressure.symbol_to_enum(str(Pressure.Units.MillimeterMercury)))
        self.assertEqual(Pressure.Units.Kilopascals, Pressure.symbol_to_enum(str(Pressure.Units.Kilopascals)))
        self.assertEqual(Pressure.Units.Pascal, Pressure.symbol_to_enum(str(Pressure.Units.Pascal)))
        self.assertEqual(Pressure.Units.Bar, Pressure.symbol_to_enum(str(Pressure.Units.Bar)))
        self.assertEqual(Pressure.Units.Millibar, Pressure.symbol_to_enum(str(Pressure.Units.Millibar)))

        try:
            Pressure.symbol_to_enum('bob')
            self.fail()
        except InvalidUnitOfMeasureException as e:
            self.assertTrue(True)
        except Exception:
            pass

    def test_to_string(self):
        a = Pressure(42)
        self.assertEqual('{} {}'.format(float(42), str(Pressure.Units.Kilopascals)), a.to_string())

    def test_to_string_format(self):
        a = Pressure(42)
        self.assertEqual('{:n} {}'.format(float(42), str(Pressure.Units.Kilopascals)), a.to_string(formatting=':n'))

    def test_to_string_enum(self):
        x = 12345.25
        a = Pressure(x, Pressure.Units.Kilopascals)
        self.assertEqual('{} {}'.format(float(x), str(Pressure.Units.Kilopascals)), a.to_string(unit=Pressure.Units.Kilopascals))

        a = Pressure(x, Pressure.Units.Millibar)
        self.assertEqual('{} {}'.format(float(x), str(Pressure.Units.Millibar)),
                         a.to_string(unit=Pressure.Units.Millibar))

        a = Pressure(x, Pressure.Units.Bar)
        self.assertEqual('{} {}'.format(float(x), str(Pressure.Units.Bar)),
                         a.to_string(unit=Pressure.Units.Bar))

        a = Pressure(x, Pressure.Units.Pascal)
        self.assertEqual('{} {}'.format(float(x), str(Pressure.Units.Pascal)),
                         a.to_string(unit=Pressure.Units.Pascal))

        a = Pressure(x, Pressure.Units.MillimeterMercury)
        self.assertEqual('{} {}'.format(float(x), str(Pressure.Units.MillimeterMercury)),
                         a.to_string(unit=Pressure.Units.MillimeterMercury))
        print('now')
        a = Pressure(x, Pressure.Units.InchesMercury)
        self.assertEqual('{} {}'.format(round(x, 2), str(Pressure.Units.InchesMercury)),
                         a.to_string(unit=Pressure.Units.InchesMercury))

    def test_to_string_format_enum(self):
        a = Pressure(42)
        self.assertEqual('{:.1f} {}'.format(float(42), str(Pressure.Units.Kilopascals)), a.to_string(formatting=':.1f', unit=Pressure.Units.Kilopascals))

    def test_abs(self):
        a = Pressure(-10)
        b = Pressure(10)
        self.assertEqual(float(10), b.abs().kilopascal)
        self.assertEqual(float(10), a.abs().kilopascal)

    def test_min(self):
        a = Pressure(-10)
        b = Pressure(10)
        self.assertEqual(float(-10), Pressure.min(a, b).kilopascal)
        self.assertEqual(float(-10), Pressure.min(b, a).kilopascal)

    def test_min_array(self):
        test_set_2 = [Pressure(10),Pressure(6),Pressure(17),Pressure(2),Pressure(31)]
        self.assertEqual(float(2), Pressure.min(array=test_set_2).kilopascal)
        test_set_2[2] = Pressure(-4)
        self.assertEqual(float(-4), Pressure.min(array=test_set_2).kilopascal)

    def test_max(self):
        a = Pressure(-10)
        b = Pressure(10)
        self.assertEqual(float(10), Pressure.max(a, b).kilopascal)
        self.assertEqual(float(10), Pressure.max(b, a).kilopascal)

    def test_max_array(self):
        test_set_2 = [Pressure(10), Pressure(6), Pressure(17), Pressure(200), Pressure(31)]
        self.assertEqual(float(200), Pressure.max(array=test_set_2).kilopascal)
        test_set_2[2] = Pressure(297)
        self.assertEqual(float(297), Pressure.max(array=test_set_2).kilopascal)

    def test_mean_array(self):
        test_set_2 = [Pressure(9), Pressure(6), Pressure(17), Pressure(2), Pressure(31)]
        self.assertEqual(13, Pressure.mean(array=test_set_2).kilopascal)
        test_set_2[3] += Pressure(-5)
        self.assertEqual(12, Pressure.mean(array=test_set_2).kilopascal)

    def test_operator(self):
        a = Pressure(90)
        b = Pressure(45)
        c = Pressure(45)
        d = Pressure(0.25)
        e = Pressure(100)

        self.assertEqual(Pressure(-90), -a, msg='unary operator')
        self.assertEqual(Pressure(135), a + b, msg='Pressure + Pressure')
        self.assertEqual(Pressure(45), a - b, msg='Pressure - Pressure')

        # in python, the "float" type has double precision, and the "int" and "long" types are one.
        self.assertEqual(1, c / b, msg='Pressure / Pressure')
        self.assertEqual(Pressure(45), a / int(2), msg='Pressure / int')
        self.assertEqual(Pressure(45), a / 2.0, msg='Pressure / float')

        self.assertEqual(Pressure(90), b * 2.0, msg='Pressure * float')
        self.assertEqual(Pressure(90), 2.0 * b, msg='float * Pressure')
        self.assertEqual(Pressure(90), b * int(2), msg='Pressure * int')
        self.assertEqual(Pressure(90), int(2) * b, msg='int * Pressure')

        self.assertEqual(Pressure(10), e % b, msg='Pressure % Pressure')

        self.assertTrue(a > b, msg='greater than')
        self.assertTrue(c >= b, msg='greater than or equal')
        self.assertTrue(b == c, msg='equal')
        self.assertTrue(c < a, msg='less than')
        self.assertTrue(c <= b, msg='less than or equal')
        self.assertTrue(a != b, msg='not equal')

