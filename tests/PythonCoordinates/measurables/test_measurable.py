import unittest
from unittest import TestCase
from PythonCoordinates.measurables.Measurable import Measurable, InvalidUnitOfMeasureException
from PythonCoordinates.measurables.physical_quantities import Length, Angle, Speed


class TestMeasurable(TestCase):
    def test_constructor(self):
        m = Measurable()
        self.assertEqual(0.0, m.underlying_value())

    def test_exceptions(self):
        try:
            l = Length('123', 'xxx')
        except InvalidUnitOfMeasureException as e:
            self.assertTrue('Invalid unit of measure' in e.message)
        except Exception:
            pass

    def test_compare_to(self):
        # python doesn't really have this
        pass

    def test_get_hash_code(self):
        m = Measurable()
        test_value = 0.0
        self.assertEqual(hash(m), hash(test_value))

    def test_equals(self):
        m1 = Measurable()
        m2 = Measurable()
        self.assertTrue(m1 == m2)
        l1 = Length(200)
        l2 = Length(250)
        self.assertFalse(l1 == l2)

    def test_is_nan(self):
        m = Measurable()
        self.assertFalse(m.is_nan())
        s = Speed(speed=float('NaN'))
        self.assertTrue(s.is_nan())



