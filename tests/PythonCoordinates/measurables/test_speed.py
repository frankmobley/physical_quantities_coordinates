import unittest
from unittest import TestCase
from PythonCoordinates.measurables.physical_quantities import Speed, Temperature, Length, Angle
from PythonCoordinates.measurables.Measurable import InvalidUnitOfMeasureException


class TestSpeed(TestCase):
    def test_conversion(self):
        reference_in_mps = 1
        mps = 1
        kilometers_per_hour = 3.600000
        knots = 1.943844
        meter_per_hour = 3600.000000
        mph = 2.236936
        kilometer_per_second = 0.001000

        reference = Speed(reference_in_mps, Speed.Units.MetersPerSecond)

        self.assertAlmostEqual(mps, reference.meters_per_second, places=5)
        self.assertAlmostEqual(kilometers_per_hour, reference.kilometers_per_hour, places=5)
        self.assertAlmostEqual(knots, reference.knots, places=5)
        self.assertAlmostEqual(meter_per_hour, reference.meters_per_hour, places=5)
        self.assertAlmostEqual(mph, reference.miles_per_hour, places=5)
        self.assertAlmostEqual(kilometer_per_second, reference.kilometers_per_second, places=5)

    def test_constructor_value_units(self):
        s = Speed(100, Speed.Units.KilometersPerHour)
        self.assertEqual(float(100), s.kilometers_per_hour)

        s = Speed(100, Speed.Units.Knots)
        self.assertEqual(float(100), s.knots)

        s = Speed(100, Speed.Units.MetersPerHour)
        self.assertEqual(float(100), s.meters_per_hour)

        s = Speed(100, Speed.Units.MilesPerHour)
        self.assertEqual(float(100), s.miles_per_hour)

        s = Speed(100, Speed.Units.KilometersPerSecond)
        self.assertEqual(float(100), s.kilometers_per_second)

        s = Speed(100, Speed.Units.MetersPerSecond)
        self.assertEqual(float(100), s.meters_per_second)

    def test_constructor_value_text(self):
        s = Speed(100, str(Speed.Units.KilometersPerHour))
        self.assertEqual(float(100), s.kilometers_per_hour)

        s = Speed(100, str(Speed.Units.Knots))
        self.assertEqual(float(100), s.knots)

        s = Speed(100, str(Speed.Units.MetersPerHour))
        self.assertEqual(float(100), s.meters_per_hour)

        s = Speed(100, str(Speed.Units.MilesPerHour))
        self.assertEqual(float(100), s.miles_per_hour)

        s = Speed(100, str(Speed.Units.KilometersPerSecond))
        self.assertEqual(float(100), s.kilometers_per_second)

        s = Speed(100, str(Speed.Units.MetersPerSecond))
        self.assertEqual(float(100), s.meters_per_second)

    def test_constructor_text_units(self):
        s = Speed('100', Speed.Units.KilometersPerHour)
        self.assertEqual(float(100), s.kilometers_per_hour)

        s = Speed('100', Speed.Units.Knots)
        self.assertEqual(float(100), s.knots)

        s = Speed('100', Speed.Units.MetersPerHour)
        self.assertEqual(float(100), s.meters_per_hour)

        s = Speed('100', Speed.Units.MilesPerHour)
        self.assertEqual(float(100), s.miles_per_hour)

        s = Speed('100', Speed.Units.KilometersPerSecond)
        self.assertEqual(float(100), s.kilometers_per_second)

        s = Speed('100', Speed.Units.MetersPerSecond)
        self.assertEqual(float(100), s.meters_per_second)

    def test_constructor_text_text(self):
        s = Speed('100', str(Speed.Units.KilometersPerHour))
        self.assertEqual(float(100), s.kilometers_per_hour)

        s = Speed('100', str(Speed.Units.Knots))
        self.assertEqual(float(100), s.knots)

        s = Speed('100', str(Speed.Units.MetersPerHour))
        self.assertEqual(float(100), s.meters_per_hour)

        s = Speed('100', str(Speed.Units.MilesPerHour))
        self.assertEqual(float(100), s.miles_per_hour)

        s = Speed('100', str(Speed.Units.KilometersPerSecond))
        self.assertEqual(float(100), s.kilometers_per_second)

        s = Speed('100', str(Speed.Units.MetersPerSecond))
        self.assertEqual(float(100), s.meters_per_second)

    def test_get_hash_code(self):
        v = 42
        a = Speed(42)
        self.assertEqual(hash(v), hash(a))

    def test_copy(self):
        a = Speed(-10)
        b = a.copy()
        self.assertTrue(a is not b)

    def test_in(self):
        x = 0.26529
        p = Speed(x)
        self.assertEqual(p.kilometers_per_hour, p.in_units(Speed.Units.KilometersPerHour))
        self.assertEqual(p.knots, p.in_units(Speed.Units.Knots))
        self.assertEqual(p.meters_per_hour, p.in_units(Speed.Units.MetersPerHour))
        self.assertEqual(p.miles_per_hour, p.in_units(Speed.Units.MilesPerHour))
        self.assertEqual(p.kilometers_per_second, p.in_units(Speed.Units.KilometersPerSecond))
        self.assertEqual(p.meters_per_second, p.in_units(Speed.Units.MetersPerSecond))

    def test_symbol_to_enum(self):
        self.assertEqual(Speed.Units.MetersPerSecond, Speed.symbol_to_enum(str(Speed.Units.MetersPerSecond)))
        self.assertEqual(Speed.Units.KilometersPerHour, Speed.symbol_to_enum(str(Speed.Units.KilometersPerHour)))
        self.assertEqual(Speed.Units.Knots, Speed.symbol_to_enum(str(Speed.Units.Knots)))
        self.assertEqual(Speed.Units.MetersPerHour, Speed.symbol_to_enum(str(Speed.Units.MetersPerHour)))
        self.assertEqual(Speed.Units.MilesPerHour, Speed.symbol_to_enum(str(Speed.Units.MilesPerHour)))
        self.assertEqual(Speed.Units.KilometersPerSecond, Speed.symbol_to_enum(str(Speed.Units.KilometersPerSecond)))

        try:
            Speed.symbol_to_enum('bob')
            self.fail()
        except InvalidUnitOfMeasureException as e:
            self.assertTrue(True)
        except Exception:
            pass

    def test_to_string(self):
        a = Speed(42)
        self.assertEqual('{} {}'.format(float(42), str(Speed.Units.MetersPerSecond)), a.to_string())

    def test_to_string_format(self):
        a = Speed(42)
        self.assertEqual('{:n} {}'.format(float(42), str(Speed.Units.MetersPerSecond)), a.to_string(formatting=':n'))

    def test_to_string_units(self):
        x = 12345.25
        a = Speed(x, Speed.Units.MetersPerSecond)
        self.assertEqual('{} {}'.format(float(x), str(Speed.Units.MetersPerSecond)),
                         a.to_string(unit=Speed.Units.MetersPerSecond))

        a = Speed(x, Speed.Units.KilometersPerHour)
        self.assertEqual('{} {}'.format(float(x), str(Speed.Units.KilometersPerHour)),
                         a.to_string(unit=Speed.Units.KilometersPerHour))

        a = Speed(x, Speed.Units.Knots)
        self.assertEqual('{} {}'.format(float(x), str(Speed.Units.Knots)),
                         a.to_string(unit=Speed.Units.Knots))

        a = Speed(x, Speed.Units.MetersPerHour)
        self.assertEqual('{} {}'.format(float(x), str(Speed.Units.MetersPerHour)),
                         a.to_string(unit=Speed.Units.MetersPerHour))

        a = Speed(x, Speed.Units.MilesPerHour)
        self.assertEqual('{} {}'.format(float(x), str(Speed.Units.MilesPerHour)),
                         a.to_string(unit=Speed.Units.MilesPerHour))

        a = Speed(x, Speed.Units.KilometersPerSecond)
        self.assertEqual('{} {}'.format(float(x), str(Speed.Units.KilometersPerSecond)),
                         a.to_string(unit=Speed.Units.KilometersPerSecond))

    def test_to_string_format_enum(self):
        x = 12345.25
        a = Speed(x, Speed.Units.MetersPerSecond)
        self.assertEqual('{:0n} {}'.format(float(x), str(Speed.Units.MetersPerSecond)),
                         a.to_string(unit=Speed.Units.MetersPerSecond, formatting=':0n'))

        a = Speed(x, Speed.Units.KilometersPerHour)
        self.assertEqual('{:1n} {}'.format(float(x), str(Speed.Units.KilometersPerHour)),
                         a.to_string(unit=Speed.Units.KilometersPerHour, formatting=':1n'))

        a = Speed(x, Speed.Units.Knots)
        self.assertEqual('{:g} {}'.format(float(x), str(Speed.Units.Knots)),
                         a.to_string(unit=Speed.Units.Knots, formatting=':g'))

        a = Speed(x, Speed.Units.MetersPerHour)
        self.assertEqual('{:0n} {}'.format(float(x), str(Speed.Units.MetersPerHour)),
                         a.to_string(unit=Speed.Units.MetersPerHour, formatting=':0n'))

        a = Speed(x, Speed.Units.MilesPerHour)
        self.assertEqual('{:.2f} {}'.format(float(x), str(Speed.Units.MilesPerHour)),
                         a.to_string(unit=Speed.Units.MilesPerHour, formatting=':.2f'))

        a = Speed(x, Speed.Units.KilometersPerSecond)
        self.assertEqual('{:.2f} {}'.format(float(x), str(Speed.Units.KilometersPerSecond)),
                         a.to_string(unit=Speed.Units.KilometersPerSecond, formatting=':.2f'))

    def test_adiabatic_speed_of_sound(self):
        s = Speed.adiabatic_speed_of_sound(Temperature.ref_temperature())
        self.assertEqual(Speed.ref_speed_of_sound(), s)

    # def test_nominal_wind_speed(self):
    #     source = CLAWSCoordinate.array_center()
    #     distance = Length(10, Length.Units.Meters)
    #     wind_speed = Speed(10, Speed.Units.Knots)
    #     wind_direction = Angle()
    #     directions = [0, 45, 90, 135, 180, 225, 270, 315]
    #     magnitude = [10, 7.1, 0, -7.1, -10, -7.1, 0, 7.1]
    #     for index in range(len(directions)):
    #         angle = Angle(directions[index], Angle.Units.Degrees)
    #         receiver = source.move(distance, angle)
    #         self.assertAlmostEqual(directions[index], source.bearing_to(receiver).degrees, places=0)
    #         temp_wind_speed = Speed.nominal_wind_speed(source_location=source, receiver_location=receiver, velocity=wind_speed, direction=wind_direction)
    #         self.assertAlmostEqual(magnitude[index], temp_wind_speed.knots, places=1)

    def test_abs(self):
        a = Speed(-42)
        b = Speed(42)
        self.assertEqual(float(42), b.abs().meters_per_second)
        self.assertEqual(float(42), a.abs().meters_per_second)

    def test_min(self):
        a = Speed(42)
        b = Speed(32)
        self.assertEqual(float(32), Speed.min(a, b).meters_per_second)
        self.assertEqual(float(32), Speed.min(b, a).meters_per_second)

    def test_min_array(self):
        test_set_2 = [Speed(10), Speed(6), Speed(17), Speed(2), Speed(31)]
        self.assertEqual(float(2), Speed.min(array=test_set_2).meters_per_second)
        test_set_2[2] = Speed(-4)
        self.assertEqual(float(-4), Speed.min(array=test_set_2).meters_per_second)

    def test_max(self):
        a = Speed(42)
        b = Speed(32)
        self.assertEqual(float(42), Speed.max(a, b).meters_per_second)
        self.assertEqual(float(42), Speed.max(b, a).meters_per_second)

    def test_max_array(self):
        test_set_2 = [Speed(10), Speed(6), Speed(174), Speed(2), Speed(31)]
        self.assertEqual(float(174), Speed.max(array=test_set_2).meters_per_second)
        test_set_2[2] = Speed(-4)
        self.assertEqual(float(31), Speed.max(array=test_set_2).meters_per_second)

    def test_mean_array(self):
        test_set_2 = [Speed(9), Speed(6), Speed(17), Speed(2), Speed(31)]
        self.assertEqual(float(13), Speed.mean(array=test_set_2).meters_per_second)
        test_set_2[3].add(-5, Speed.Units.MetersPerSecond)
        self.assertEqual(float(12), Speed.mean(array=test_set_2).meters_per_second)

    def test_operator(self):
        a = Speed(90)
        b = Speed(45)
        c = Speed(45)
        d = Speed(55)

        self.assertEqual(Speed(-90), -a, msg='unary operator')
        self.assertEqual(Speed(135), a + b, msg='Speed + Speed')
        self.assertEqual(Speed(45), a - b, msg='Speed - Speed')

        # in python, the "float" type has double precision, and the "int" and "long" types are one.
        self.assertEqual(1, c / b, msg='Speed / Speed')
        self.assertEqual(Speed(45), a / int(2), msg='Speed / int')
        self.assertEqual(Speed(45), a / 2.0, msg='Speed / float')

        self.assertEqual(Speed(90), b * 2.0, msg='Speed * float')
        self.assertEqual(Speed(90), 2.0 * b, msg='float * Speed')
        self.assertEqual(Speed(90), b * int(2), msg='Speed * int')
        self.assertEqual(Speed(90), int(2) * b, msg='int * Speed')

        self.assertEqual(Speed(10), d % c, msg='Speed % Speed')

        self.assertTrue(a > b, msg='greater than')
        self.assertTrue(c >= b, msg='greater than or equal')
        self.assertTrue(b == c, msg='equal')
        self.assertTrue(c < a, msg='less than')
        self.assertTrue(c <= b, msg='less than or equal')
        self.assertTrue(a != b, msg='not equal')
