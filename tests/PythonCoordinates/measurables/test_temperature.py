import unittest
from unittest import TestCase
from PythonCoordinates.measurables.physical_quantities import Temperature
from PythonCoordinates.measurables.Measurable import InvalidUnitOfMeasureException


class TestTemperature(TestCase):
    def test_constructor_value_units(self):
        s = Temperature(32, Temperature.Units.Fahrenheit)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(0), s.celsius, places=10)

        s = Temperature(32, Temperature.Units.Fahrenheit)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(32), s.fahrenheit, places=10)

        s = Temperature(100, Temperature.Units.Kelvin)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(100), s.kelvin, places=10)

        s = Temperature(-10, Temperature.Units.Celsius)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(-10), s.celsius, places=10)

        s = Temperature(70, Temperature.Units.Rankine)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(70), s.rankine, places=10)

        self.assertAlmostEqual(Temperature(0, Temperature.Units.Kelvin).kelvin, Temperature(0, Temperature.Units.Rankine).kelvin, places=12)

    def test_constructor_value_text(self):
        s = Temperature(32, str(Temperature.Units.Fahrenheit))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(32), s.fahrenheit, places=10)

        s = Temperature(100, str(Temperature.Units.Kelvin))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(100), s.kelvin, places=10)

        s = Temperature(-10, str(Temperature.Units.Celsius))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(-10), s.celsius, places=10)

        s = Temperature(70, str(Temperature.Units.Rankine))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(70), s.rankine, places=10)

    def test_constructor_text_units(self):
        s = Temperature('32', Temperature.Units.Fahrenheit)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(32), s.fahrenheit, places=10)

        s = Temperature('100', Temperature.Units.Kelvin)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(100), s.kelvin, places=10)

        s = Temperature('-10', Temperature.Units.Celsius)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(-10), s.celsius, places=10)

        s = Temperature('70', Temperature.Units.Rankine)
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(70), s.rankine, places=10)

    def test_constructor_text_text(self):
        s = Temperature('32', str(Temperature.Units.Fahrenheit))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(32), s.fahrenheit, places=10)

        s = Temperature('100', str(Temperature.Units.Kelvin))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(100), s.kelvin, places=10)

        s = Temperature('-10', str(Temperature.Units.Celsius))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(-10), s.celsius, places=10)

        s = Temperature('70', str(Temperature.Units.Rankine))
        # NOTE: change the following to assertAlmostEqual from assertEqual
        self.assertAlmostEqual(float(70), s.rankine, places=10)

    def test_get_hash_code(self):
        v = 34.6
        a = Temperature(34.6, Temperature.Units.Kelvin)
        self.assertEqual(hash(v), hash(a))

    def test_copy(self):
        a = Temperature(45, Temperature.Units.Celsius)
        b = a.copy()
        self.assertTrue(a is not b)

    def test_in(self):
        x = 110.26529
        p = Temperature(x, Temperature.Units.Celsius)
        self.assertEqual(p.celsius, p.in_units(Temperature.Units.Celsius))
        self.assertEqual(p.kelvin, p.in_units(Temperature.Units.Kelvin))
        self.assertEqual(p.fahrenheit, p.in_units(Temperature.Units.Fahrenheit))
        self.assertEqual(p.rankine, p.in_units(Temperature.Units.Rankine))

    def test_symbol_to_enum(self):
        self.assertEqual(Temperature.Units.Celsius, Temperature.symbol_to_enum(str(Temperature.Units.Celsius)))
        self.assertEqual(Temperature.Units.Kelvin, Temperature.symbol_to_enum(str(Temperature.Units.Kelvin)))
        self.assertEqual(Temperature.Units.Fahrenheit, Temperature.symbol_to_enum(str(Temperature.Units.Fahrenheit)))
        self.assertEqual(Temperature.Units.Rankine, Temperature.symbol_to_enum(str(Temperature.Units.Rankine)))

        try:
            Temperature.symbol_to_enum('bob')
            self.fail()
        except InvalidUnitOfMeasureException as e:
            self.assertTrue(True)
        except Exception:
            pass

    def test_to_string(self):
        a = Temperature(77, Temperature.Units.Kelvin)
        self.assertEqual('{}°{}'.format(float(77), str(Temperature.Units.Kelvin)), a.to_string())

    def test_to_string_format(self):
        a = Temperature(65, Temperature.Units.Kelvin)
        self.assertEqual('{:0n}°{}'.format(float(65), str(Temperature.Units.Kelvin)), a.to_string(formatting=':0n'))

    def test_to_string_units(self):
        a = Temperature(25, Temperature.Units.Celsius)
        self.assertEqual('{:.2f}°{}'.format(float(25), str(Temperature.Units.Celsius)),
                         a.to_string(unit=Temperature.Units.Celsius, formatting=':.2f'))

        a = Temperature(-10, Temperature.Units.Fahrenheit)
        self.assertEqual('{:.2f}°{}'.format(float(-10), str(Temperature.Units.Fahrenheit)),
                         a.to_string(unit=Temperature.Units.Fahrenheit, formatting=':.2f'))

        a = Temperature(65, Temperature.Units.Rankine)
        self.assertEqual('{:.2f}°{}'.format(float(65), str(Temperature.Units.Rankine)),
                         a.to_string(unit=Temperature.Units.Rankine, formatting=':.2f'))

        a = Temperature(65, Temperature.Units.Kelvin)
        self.assertEqual('{:.2f}°{}'.format(float(65), str(Temperature.Units.Kelvin)),
                         a.to_string(unit=Temperature.Units.Kelvin, formatting=':.2f'))

    def test_to_string_format_units(self):
        a = Temperature(25, Temperature.Units.Celsius)
        self.assertEqual('{:1n}°{}'.format(float(25), str(Temperature.Units.Celsius)),
                         a.to_string(unit=Temperature.Units.Celsius, formatting=':1n'))

        a = Temperature(-10, Temperature.Units.Fahrenheit)
        self.assertEqual('{:g}°{}'.format(float(-10), str(Temperature.Units.Fahrenheit)),
                         a.to_string(unit=Temperature.Units.Fahrenheit, formatting=':g'))

        a = Temperature(65, Temperature.Units.Rankine)
        self.assertEqual('{:0n}°{}'.format(float(65), str(Temperature.Units.Rankine)),
                         a.to_string(unit=Temperature.Units.Rankine, formatting=':0n'))

        a = Temperature(65, Temperature.Units.Kelvin)
        self.assertEqual('{:.2f}°{}'.format(float(65), str(Temperature.Units.Kelvin)),
                         a.to_string(unit=Temperature.Units.Kelvin, formatting=':.2f'))

    def test_min(self):
        a = Temperature(10, Temperature.Units.Kelvin)
        b = Temperature(26, Temperature.Units.Kelvin)
        self.assertEqual(a, Temperature.min(a, b))
        self.assertEqual(a, Temperature.min(b, a))

    def test_min_array(self):
        test_set_2 = [Temperature(10, Temperature.Units.Kelvin), Temperature(6, Temperature.Units.Kelvin), Temperature(17, Temperature.Units.Kelvin), Temperature(2, Temperature.Units.Kelvin), Temperature(31, Temperature.Units.Kelvin)]
        self.assertEqual(float(2), Temperature.min(array=test_set_2).kelvin)
        test_set_2[2] = Temperature(0, Temperature.Units.Kelvin)
        self.assertEqual(float(0), Temperature.min(array=test_set_2).kelvin)

    def test_max(self):
        a = Temperature(2, Temperature.Units.Kelvin)
        b = Temperature(10, Temperature.Units.Kelvin)
        self.assertEqual(b, Temperature.max(a, b))
        self.assertEqual(b, Temperature.max(b, a))

    def test_max_array(self):
        test_set_2 = [Temperature(10, Temperature.Units.Kelvin), Temperature(60.2, Temperature.Units.Kelvin), Temperature(17, Temperature.Units.Kelvin), Temperature(2, Temperature.Units.Kelvin), Temperature(31, Temperature.Units.Kelvin)]
        self.assertEqual(test_set_2[1], Temperature.max(array=test_set_2))
        test_set_2[2] = Temperature(100, Temperature.Units.Kelvin)
        self.assertEqual(test_set_2[2], Temperature.max(array=test_set_2))

    def test_mean_array(self):
        test_set_2 = [Temperature(9, Temperature.Units.Kelvin), Temperature(6, Temperature.Units.Kelvin), Temperature(17, Temperature.Units.Kelvin), Temperature(7, Temperature.Units.Kelvin), Temperature(31, Temperature.Units.Kelvin)]

        self.assertEqual(float(14), Temperature.mean(array=test_set_2).kelvin)

        test_set_2[3] = Temperature(test_set_2[3].kelvin -5, Temperature.Units.Kelvin)

        self.assertEqual(float(13), Temperature.mean(array=test_set_2).kelvin)

    def test_operator(self):
        a = Temperature(90, Temperature.Units.Kelvin)
        b = Temperature(45, Temperature.Units.Kelvin)
        c = Temperature(45, Temperature.Units.Kelvin)
        d = Temperature(135, Temperature.Units.Kelvin)

        self.assertEqual(Temperature(-90, Temperature.Units.Kelvin), -a, msg='unary operator')
        self.assertEqual(Temperature(135, Temperature.Units.Kelvin), a + b, msg='Temperature + Temperature')
        self.assertEqual(Temperature(45, Temperature.Units.Kelvin), a - b, msg='Temperature - Temperature')

        # in python, the "float" type has double precision, and the "int" and "long" types are one.
        self.assertEqual(1, c / b, msg='Temperature / Temperature')
        self.assertEqual(b, a / int(2), msg='Temperature / int')
        self.assertEqual(c, a / 2.0, msg='Temperature / float')

        self.assertEqual(a, b * 2.0, msg='Temperature * float')
        self.assertEqual(a, 2.0 * b, msg='float * Temperature')
        self.assertEqual(a, b * int(2), msg='Temperature * int')
        self.assertEqual(a, int(2) * b, msg='int * Temperature')

        self.assertEqual(Temperature(5, Temperature.Units.Kelvin), (b % 10.0), msg='Temperature % float')

        self.assertTrue(a > b, msg='greater than')
        self.assertTrue(c >= b, msg='greater than or equal')
        self.assertTrue(d >= b, msg='greater than or equal')
        self.assertTrue(b == c, msg='equal')
        self.assertTrue(c < a, msg='less than')
        self.assertTrue(c <= b, msg='less than or equal')
        self.assertTrue(c <= d, msg='less than or equal')
        self.assertTrue(a != b, msg='not equal')

