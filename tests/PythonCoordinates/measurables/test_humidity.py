import unittest
from unittest import TestCase
from PythonCoordinates.measurables.physical_quantities import Humidity
from PythonCoordinates.measurables.Measurable import InvalidUnitOfMeasureException


class TestHumidity(TestCase):
    def test_constructor_humidity_default(self):
        h = Humidity()
        self.assertEqual(0.0, h.percentage)

    def test_constructor_humidity_value_units(self):
        h = Humidity(25, Humidity.Units.Percentage)
        self.assertEqual(float(25), h.percentage)

    def test_constructor_humidity_text_units(self):
        h = Humidity('25', Humidity.Units.Percentage)
        self.assertEqual(float(25), h.percentage)

    def test_constructor_humidity_text_text(self):
        h = Humidity('25', str(Humidity.Units.Percentage))
        self.assertEqual(float(25), h.percentage)

    def test_constructor_humidity_value_text(self):
        h = Humidity(25, str(Humidity.Units.Percentage))
        self.assertEqual(float(25), h.percentage)

    def test_get_hash_code(self):
        v = 25
        a = Humidity(v)
        self.assertEqual(hash(v), hash(a))

    def test_copy(self):
        a = Humidity(25)
        b = a.copy()
        self.assertTrue(a is not b)

    def test_in(self):
        x = 0.26529
        h = Humidity(x)
        self.assertEqual(h.percentage, h.in_units(Humidity.Units.Percentage))

    def test_symbol_to_enum(self):
        self.assertEqual(Humidity.Units.Percentage, Humidity.symbol_to_enum(str(Humidity.Units.Percentage)))
        try:
            Humidity.symbol_to_enum('bob')
            self.fail()
        except InvalidUnitOfMeasureException as e:
            self.assertTrue(True)
        except Exception:
            pass

    def test_to_string_default(self):
        h = Humidity(42)
        self.assertEqual('{} '.format(float(42)) + str(Humidity.Units.Percentage), h.to_string())

    def test_to_string_units(self):
        h = Humidity(42)
        self.assertEqual('{} '.format(float(42)) + str(Humidity.Units.Percentage), h.to_string(Humidity.Units.Percentage))

    def test_to_string_format(self):
        h = Humidity(42)
        self.assertEqual('{:n} '.format(float(42)) + str(Humidity.Units.Percentage), h.to_string(formatting=':n'))

    def test_to_string_format_units(self):
        h = Humidity(42)
        self.assertEqual('{:n} '.format(42) + str(Humidity.Units.Percentage), h.to_string(unit=Humidity.Units.Percentage, formatting=':n'))

    def test_min(self):
        a = Humidity(10)
        b = Humidity(26)
        self.assertEqual(a, Humidity.min(a, b))
        self.assertEqual(a, Humidity.min(b, a))

    def test_min_array(self):
        test_set_2 = [Humidity(10),Humidity(6),Humidity(17),Humidity(2),Humidity(31)]
        self.assertEqual(2, Humidity.min(array=test_set_2).percentage)
        test_set_2[2] = Humidity(0)
        self.assertEqual(0, Humidity.min(array=test_set_2).percentage)

    def test_max(self):
        a = Humidity(2)
        b = Humidity(10)
        self.assertEqual(b, Humidity.max(a, b))
        self.assertEqual(b, Humidity.max(b, a))

    def test_max_array(self):
        test_set_2 = [Humidity(10), Humidity(60.2), Humidity(17), Humidity(2), Humidity(31)]
        self.assertEqual(test_set_2[1], Humidity.max(array=test_set_2))
        test_set_2[2] = Humidity(100)
        self.assertEqual(test_set_2[2], Humidity.max(array=test_set_2))

    def test_mean_array(self):
        test_set_2 = [Humidity(9), Humidity(6), Humidity(17), Humidity(7), Humidity(31)]
        self.assertEqual(14, Humidity.mean(array=test_set_2).percentage)
        test_set_2[3] -= Humidity(5)
        self.assertEqual(13, Humidity.mean(array=test_set_2).percentage)

    def test_operator(self):
        a = Humidity(90)
        b = Humidity(45)
        c = Humidity(45)
        d = Humidity(0.25)
        e = Humidity(80)

        self.assertEqual(Humidity(45.25), d + c, msg='Humidity + Humidity')
        self.assertEqual(Humidity(45), a - b, msg='Humidity - Humidity')

        # in python, the "float" type has double precision, and the "int" and "long" types are one.
        self.assertEqual(1, c / b, msg='Humidity / Humidity')
        self.assertEqual(Humidity(45), a / int(2), msg='Humidity / int')
        self.assertEqual(Humidity(45), a / 2.0, msg='Humidity / float')

        self.assertEqual(Humidity(90), b * 2.0, msg='Humidity * float')
        self.assertEqual(Humidity(90), 2.0 * b, msg='float * Humidity')
        self.assertEqual(Humidity(90), b * int(2), msg='Humidity * int')
        self.assertEqual(Humidity(90), int(2) * b, msg='int * Humidity')

        self.assertEqual(Humidity(10), a % e, msg='Humidity % Humidity')

        self.assertTrue(a > b, msg='greater than')
        self.assertTrue(c >= b, msg='greater than or equal')
        self.assertTrue(b == c, msg='equal')
        self.assertTrue(c < a, msg='less than')
        self.assertTrue(c <= b, msg='less than or equal')
        self.assertTrue(a != b, msg='not equal')

