import math
from unittest import TestCase
from PythonCoordinates.measurables.physical_quantities import Angle, Length
from PythonCoordinates.measurables.Measurable import InvalidUnitOfMeasureException


class TestAngle(TestCase):
    test_set_2 = [Angle(10), Angle(5), Angle(350), Angle(355), Angle(0)]

    def test_angle_from(self):
        a = Angle()
        self.assertEqual(0, a.degrees)

    def test_angle_from_double_enum(self):
        a = Angle(45)
        self.assertEqual(float(45), a.degrees, msg='default units inconclusive')

        b = Angle(45, Angle.Units.Degrees)
        self.assertEqual(float(45), b.degrees, msg='Degree units inconclusive')

        c = Angle(math.pi, Angle.Units.Radians)
        self.assertAlmostEqual(float(180), c.degrees, places=8, msg='radian units inconclusive')

        d = Angle(math.pi * 500, Angle.Units.Milliradians)
        self.assertAlmostEqual(float(90), d.degrees, places=8, msg='milliradians units inconclusive')

    def test_angle_from_text_enum(self):
        b = Angle('45', Angle.Units.Degrees)
        self.assertEqual(float(45), b.degrees, msg='Degree units inconclusive')

        c = Angle(str(math.pi), Angle.Units.Radians)
        self.assertAlmostEqual(float(180), c.degrees, places=8, msg='radian units inconclusive')

        d = Angle(str(math.pi * 1000), Angle.Units.Milliradians)
        self.assertAlmostEqual(float(180), d.degrees, places=8, msg='milliradians units inconclusive')

    def test_angle_from_text_text(self):
        b = Angle('45', str(Angle.Units.Degrees))
        self.assertEqual(float(45), b.degrees, msg='Degree units inconclusive')

        c = Angle(str(math.pi), str(Angle.Units.Radians))
        self.assertAlmostEqual(float(180), c.degrees, places=8, msg='radian units inconclusive')

        d = Angle(str(math.pi * 1000), str(Angle.Units.Milliradians))
        self.assertAlmostEqual(float(180), d.degrees, places=8, msg='milliradians units inconclusive')

    def test_angle_from_double_text(self):
        b = Angle(45, str(Angle.Units.Degrees))
        self.assertEqual(float(45), b.degrees, msg='Degree units inconclusive')

        c = Angle(math.pi, str(Angle.Units.Radians))
        self.assertAlmostEqual(float(180), c.degrees, places=8, msg='radian units inconclusive')

        d = Angle(math.pi * 500, str(Angle.Units.Milliradians))
        self.assertAlmostEqual(float(90), d.degrees, places=8, msg='milliradians units inconclusive')

    def test_angle_from_compass(self):
        a = Angle('NE')
        self.assertEqual(float(45), a.degrees, msg='NE = 45')

        b = Angle('W')
        self.assertEqual(float(270), b.degrees, msg='W = 270')

        c = Angle('SSE')
        self.assertEqual(float(157.5), c.degrees, msg='SSE = 157.5')

        d = Angle('CALM')
        self.assertEqual(float(0), d.degrees, msg='CALM = 0')

    def test_conversion(self):
        reference_in_radians = 1
        degrees = Angle.deg_per_rad()
        milliradians = 1000
        reference = Angle(reference_in_radians, Angle.Units.Radians)
        self.assertAlmostEqual(degrees, reference.degrees, places=5)
        self.assertAlmostEqual(milliradians, reference.milliradians, places=5)

    def test_get_hash_code(self):
        v = 5.67
        a = Angle(v)
        self.assertEqual(hash(v), hash(a))

    def test_copy(self):
        a = Angle(65)
        b = a.copy()
        self.assertTrue(a is not b)

    def test_in(self):
        x = 0.26529
        a = Angle(x, Angle.Units.Radians)
        self.assertEqual(a.degrees, a.in_units(Angle.Units.Degrees))
        self.assertEqual(a.radians, a.in_units(Angle.Units.Radians))
        self.assertEqual(a.milliradians, a.in_units(Angle.Units.Milliradians))

    def test_symbol_to_enum(self):
        self.assertEqual(Angle.Units.Degrees, Angle.symbol_to_enum(str(Angle.Units.Degrees)))
        self.assertEqual(Angle.Units.Radians, Angle.symbol_to_enum(str(Angle.Units.Radians)))
        self.assertEqual(Angle.Units.Milliradians, Angle.symbol_to_enum(str(Angle.Units.Milliradians)))

        try:
            Angle.symbol_to_enum('bob')
            self.fail()
        except InvalidUnitOfMeasureException as e:
            self.assertTrue(True)
        except Exception:
            pass

    def test_to_string(self):
        a = Angle(45.2)
        self.assertEqual('45.2 deg', a.to_string())

    def test_to_string_enum(self):
        a = Angle(45.2)
        self.assertEqual('45.2 deg', a.to_string(unit=Angle.Units.Degrees))

        b = Angle(1.5, Angle.Units.Radians)
        self.assertEqual('1.5 rad', b.to_string(unit=Angle.Units.Radians))

        c = Angle(1000, Angle.Units.Milliradians)
        self.assertEqual('{} mrad'.format(float(1000)), c.to_string(unit=Angle.Units.Milliradians))

    def test_to_string_format(self):
        a = Angle(45.2)
        self.assertEqual('45.200 deg', a.to_string(formatting=':.3f'))

    def test_to_string_format_enum(self):
        a = Angle(45.21935)
        self.assertEqual('45.2 deg', a.to_string(formatting=':.1f', unit=Angle.Units.Degrees))

        b = Angle(1.576529, Angle.Units.Radians)
        self.assertEqual('1.58 rad', b.to_string(formatting=':.2f', unit=Angle.Units.Radians))

        c = Angle(500, Angle.Units.Milliradians)
        self.assertEqual('500.00 mrad', c.to_string(formatting=':.2f', unit=Angle.Units.Milliradians))

    def test_abs(self):
        a = Angle(1.576529, Angle.Units.Radians)
        b = Angle(-1.576529, Angle.Units.Radians)
        self.assertEqual(a, b.abs())

    def test_sin(self):
        x = 1.576529
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.sin(), math.sin(x), places=10)

        x = 0
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.sin(), math.sin(x), places=10)

        x = 0 - 1.576529
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.sin(), math.sin(x), places=10)

        x = math.pi / 4
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.sin(), math.sin(x), places=10)

    def test_cos(self):
        x = 1.576529
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.cos(), math.cos(x), places=10)

        x = 0
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.cos(), math.cos(x), places=10)

        x = 0 - 1.576529
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.cos(), math.cos(x), places=10)

        x = math.pi / 4
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.cos(), math.cos(x), places=10)

    def test_tan(self):
        x = 0.26529
        a = Angle(x, Angle.Units.Radians)
        self.assertAlmostEqual(a.tan(), math.tan(x), places=10)

    def test_asin(self):
        x = 0.5
        a = Angle.asin(math.sin(x))
        self.assertAlmostEqual(a.radians, x, places=10)

    def test_acos(self):
        x = 0.433
        a = Angle.acos(math.cos(x))
        self.assertAlmostEqual(a.radians, x, places=10)

    def test_atan(self):
        x = 0.153
        a = Angle.atan(math.tan(x))
        self.assertAlmostEqual(a.radians, x, places=10)

    def test_atan2_double(self):
        x = 0.433
        y = 0.433
        self.assertAlmostEqual(Angle.atan2(y, x).radians, Angle(45).radians, places=10)
        self.assertAlmostEqual(Angle.atan2(-y, x).radians, Angle(-45).radians, places=10)

    def test_atan2_length(self):
        x = Length(0.433)
        y = Length(0.433)
        self.assertAlmostEqual(Angle.atan2(y, x).radians, Angle(45).radians, places=10)
        self.assertAlmostEqual(Angle.atan2(-y, x).radians, Angle(-45).radians, places=10)

    def test_to_compass(self):
        self.assertEqual(Angle(0), Angle(90).to_compass(), msg='90 cart = N = 0')
        self.assertEqual(Angle(180), Angle(-90).to_compass(), msg='-90 cart = S = 180')

    def test_to_compass_point(self):
        a = Angle(90)
        self.assertEqual('E', a.to_compass_point())

        a = Angle(100)
        self.assertEqual('E', a.to_compass_point())
        print('N')
        a = Angle(358)
        self.assertEqual('N', a.to_compass_point())
        print('SW')
        a = Angle(225)
        self.assertEqual('SW', a.to_compass_point())
        print('WNW')
        a = Angle(292)
        self.assertEqual('WNW', a.to_compass_point())

    def test_compass_point_to_degrees(self):
        self.assertEqual(Angle(90), Angle.compass_point_to_degrees('E'))
        self.assertEqual(Angle(0), Angle.compass_point_to_degrees('N'))
        self.assertEqual(Angle(135), Angle.compass_point_to_degrees('SE'))
        self.assertEqual(Angle(337.5), Angle.compass_point_to_degrees('NNW'))

    def test_normalized(self):
        self.assertEqual(Angle(45), Angle(45+360).normalized)
        self.assertEqual(Angle(184), Angle(184 + 720).normalized)

    def test_normalize(self):
        a = Angle(-90, Angle.Units.Degrees)
        a.normalize()
        self.assertEqual(float(270), a.degrees)

        b = Angle(361, Angle.Units.Degrees)
        b.normalize()
        self.assertEqual(float(1), b.degrees)

    def test_min(self):
        a = Angle(225)
        b = Angle(90)
        self.assertEqual(b, Angle.min(a, b))
        self.assertEqual(b, Angle.min(b, a))

    def test_min_array(self):
        self.assertEqual(self.test_set_2[4], Angle.min(array=self.test_set_2))

    def test_max(self):
        a = Angle(225)
        b = Angle(90)
        self.assertEqual(a, Angle.max(a, b))
        self.assertEqual(a, Angle.max(b, a))

    def test_max_array(self):
        self.assertEqual(self.test_set_2[3], Angle.max(array=self.test_set_2))

    def test_mean_array(self):
        print(Angle(0).normalized.value)
        print(Angle.mean(array=self.test_set_2).normalized.value)
        self.assertEqual(Angle(0).normalized, Angle.mean(array=self.test_set_2).normalized)

    def test_min_normalized_difference(self):
        a = Angle(355)
        b = Angle(5)
        self.assertEqual(Angle(10), a.min_normalized_difference(b))
        self.assertEqual(Angle(10), b.min_normalized_difference(a))

    def test_operator(self):
        a = Angle(90)
        b = Angle(45)
        c = Angle(45)
        d = Angle(0.25)
        e = Angle(65)

        self.assertEqual(Angle(-90), -a, msg='unary operator')
        self.assertEqual(Angle(135), a + b, msg='Angle + Angle')
        self.assertEqual(Angle(45), a - b, msg='Angle - Angle')

        # in python, the "float" type has double precision, and the "int" and "long" types are one.
        self.assertEqual(1, c / b, msg='Angle / Angle')
        self.assertEqual(Angle(45), a / int(2), msg='Angle / int')
        self.assertEqual(Angle(45), a / 2.0, msg='Angle / float')

        self.assertEqual(Angle(90), b * 2.0, msg='Angle * float')
        self.assertEqual(Angle(90), 2.0 * b, msg='float * Angle')
        self.assertEqual(Angle(90), b * int(2), msg='Angle * int')
        self.assertEqual(Angle(90), int(2) * b, msg='int * Angle')

        self.assertEqual(Angle(20), e % b, msg='Angle % Angle')

        self.assertTrue(a > b, msg='greater than')
        self.assertTrue(c >= b, msg='greater than or equal')
        self.assertTrue(b == c, msg='equal')
        self.assertTrue(c < a, msg='less than')
        self.assertTrue(c <= b, msg='less than or equal')
        self.assertTrue(a != b, msg='not equal')
