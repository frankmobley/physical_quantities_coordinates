ECHO OFF
REM This file demonstrates the methods required to evaluate the coverage of the tests for the provided
REM python files.
REM
REM
REM Delete any pre-existing coverage analyses that have been collected
REM
coverage erase
REM
REM Coverage calculation is conducted by running the various tests included in this folder
REM
coverage run -m unittest test_angle.py 
REM
REM	We add the '-a' operator to append this coverage analysis to the previous analysis
REM
coverage run -a -m unittest test_humidity.py
REM
REM	We add the '-a' operator to append this coverage analysis to the previous analysis
REM
coverage run -a -m unittest test_length.py
REM
REM	We add the '-a' operator to append this coverage analysis to the previous analysis
REM
coverage run -a -m unittest test_measurable.py
REM
REM	We add the '-a' operator to append this coverage analysis to the previous analysis
REM
coverage run -a -m unittest test_pressure.py
coverage run -a -m unittest test_speed.py
coverage run -a -m unittest test_temperature.py
REM
rem	For this report we will excluded any elements from this folder that contain the "_test.py" post-fix and the 
REM __init__.py file. The include statement lists all the files that are not excluded that are Python files
REM within this folder.
coverage report --omit=./*_test.py,__init__.py --include=./*.py
