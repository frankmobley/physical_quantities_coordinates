from unittest import TestCase
from PythonCoordinates.measurables.physical_quantities import Length, Speed
from datetime import timedelta


class TestLength(TestCase):
    def test_conversion(self):
        tolerance = 1e-5
        reference_in_meters = 1
        feet = 1.0 / Length.meters_per_foot()
        miles = 1.0 / Length.meters_per_foot() / Length.feet_per_mile()
        nautical_miles = 1.0 / Length.meters_per_nm()
        kilometer = 1.0 / 1000.0

        reference = Length(reference_in_meters, Length.Units.Meters)

        self.assertAlmostEqual(feet, reference.feet, places=5)
        self.assertAlmostEqual(miles, reference.statute_miles, places=5)
        self.assertAlmostEqual(nautical_miles, reference.nautical_miles, places=5)
        self.assertAlmostEqual(kilometer, reference.kilometers, places=5)

    def test_length_default_constructor(self):
        a = Length()
        self.assertEqual(a.underlying_value(), 0)

    def test_length_constructor_double_units(self):
        a = Length(5)
        self.assertEqual(5, a.meters)
        b = Length(5, Length.Units.Feet)
        self.assertEqual(5, b.feet)
        c = Length(10, Length.Units.Meters)
        self.assertEqual(10, c.meters)
        d = Length(50, Length.Units.NauticalMiles)
        self.assertEqual(50, d.nautical_miles)
        e = Length(15, Length.Units.StatuteMiles)
        self.assertEqual(15, e.statute_miles)

    def test_length_constructor_text_units(self):
        a = Length('5', Length.Units.Feet)
        self.assertEqual(5, a.feet)
        b = Length('10', Length.Units.Meters)
        self.assertEqual(10, b.meters)
        c = Length('50', Length.Units.NauticalMiles)
        self.assertEqual(50, c.nautical_miles)
        d = Length('101', Length.Units.StatuteMiles)
        self.assertEqual(101, d.statute_miles)

    def test_length_constructor_double_text(self):
        a = Length(5, str(Length.Units.Feet))
        self.assertEqual(5, a.feet)
        b = Length(10, str(Length.Units.Meters))
        self.assertEqual(10, b.meters)
        c = Length(50, str(Length.Units.NauticalMiles))
        self.assertEqual(50, c.nautical_miles)
        d = Length(101, str(Length.Units.StatuteMiles))
        self.assertEqual(101, d.statute_miles)

    def test_length_constructor_text_text(self):
        a = Length('5', str(Length.Units.Feet))
        self.assertEqual(5, a.feet)
        b = Length('10', str(Length.Units.Meters))
        self.assertEqual(10, b.meters)
        c = Length('50', str(Length.Units.NauticalMiles))
        self.assertEqual(50, c.nautical_miles)
        d = Length('101', str(Length.Units.StatuteMiles))
        self.assertEqual(101, d.statute_miles)

    def test_equals(self):
        a = Length(100)
        b = Length(100)
        self.assertEqual(a, b)

    def test_get_hash_code(self):
        x = 250
        l = Length(250)
        self.assertEqual(x.__hash__(), l.__hash__())

    def test_compare_to(self):
        # python doesn't have this
        return

    def test_add_meters(self):
        # I did NOT include specific "add" variants in my class, ONLY one which allows you to put units.
        a = Length(100)
        a.add(100, Length.Units.Meters)
        self.assertEqual(200, a.meters)

    def test_add_feet(self):
        # I did NOT include specific "add" variants in my class, ONLY one which allows you to put units.
        a = Length(100, Length.Units.Feet)
        a.add(100, Length.Units.Feet)
        self.assertEqual(200, a.feet)

    def test_add(self):
        a = Length(200, Length.Units.Feet)
        a.add(150, Length.Units.Feet)
        self.assertEqual(350, a.feet)

    def test_to_array_of_meters(self):
        length_list = []
        numbers = [-2.0, -1.6, -1.2, -0.8, -0.4, 0.0, 0.4, 0.8, 1.2, 1.6]
        for index in range(len(numbers)):
            length_list.append(Length(numbers[index]))
        result = Length.to_array_of_meters(length_list)
        self.assertEqual(len(length_list), len(result))
        for index in range(len(numbers)):
            self.assertEqual(length_list[index].meters, result[index])

    def test_to_string_default(self):
        a = Length(42)
        self.assertEqual('{} {}'.format(float(42), str(Length.Units.Meters)), a.to_string())

    def test_to_string_units(self):
        x = 12345.25
        a = Length(x)
        self.assertEqual('{:n} {}'.format(x, str(Length.Units.Meters)),
                         a.to_string(unit=Length.Units.Meters, formatting=':n'))
        b = Length(x, Length.Units.Feet)
        self.assertEqual('{:n} {}'.format(x, str(Length.Units.Feet)),
                         b.to_string(unit=Length.Units.Feet, formatting=':n'))
        c = Length(x, Length.Units.NauticalMiles)
        self.assertEqual('{:g} {}'.format(x, str(Length.Units.NauticalMiles)),
                         c.to_string(unit=Length.Units.NauticalMiles, formatting=':g'))
        d = Length(x, Length.Units.StatuteMiles)
        self.assertEqual('{:.2f} {}'.format(x, str(Length.Units.StatuteMiles)),
                         d.to_string(unit=Length.Units.StatuteMiles, formatting=':.2f'))

    def test_abs(self):
        a = Length(-10)
        b = Length(10)
        self.assertEqual(10, a.abs().meters)
        self.assertEqual(10, b.abs().meters)

    def test_min(self):
        a = Length(-10)
        b = Length(10)
        self.assertEqual(a, Length.min(a=a, b=b))
        self.assertEqual(a, Length.min(a=b, b=a))

    def test_min_list(self):
        length_list = []
        length_list.append(Length(10))
        length_list.append(Length(6))
        length_list.append(Length(17))
        length_list.append(Length(2))
        length_list.append(Length(31))
        self.assertEqual(2, Length.min(array=length_list).meters)
        length_list[2] = Length(-4)
        self.assertEqual(-4, Length.min(array=length_list).meters)

    def test_max(self):
        a = Length(-10)
        b = Length(10)
        self.assertEqual(b, Length.max(a=a, b=b))
        self.assertEqual(b, Length.max(a=b, b=a))

    def test_max_list(self):
        length_list = []
        length_list.append(Length(10))
        length_list.append(Length(60.2))
        length_list.append(Length(17))
        length_list.append(Length(2))
        length_list.append(Length(31))
        self.assertEqual(length_list[1], Length.max(array=length_list))
        length_list[2] = Length(400)
        self.assertEqual(length_list[2], Length.max(array=length_list))

    def test_mean_list(self):
        length_list = []
        length_list.append(Length(9))
        length_list.append(Length(6))
        length_list.append(Length(17))
        length_list.append(Length(2))
        length_list.append(Length(31))
        self.assertEqual(13, Length.mean(array=length_list).meters)
        length_list[3].add(-5)
        self.assertEqual(12, Length.mean(array=length_list).meters)

    def test_copy(self):
        a = Length(-10)
        b = a.copy()
        self.assertTrue(a is not b)

    def test_operator(self):
        a = Length(90)
        b = Length(45)
        c = Length(45)
        d = Length(0.25)
        e = Length(25)

        self.assertEqual(Length(-90), -a, msg='unary operator')
        self.assertEqual(Length(135), a + b, msg='Length + Length')
        self.assertEqual(Length(45), a - b, msg='Length - Length')

        # in python, the "float" type has double precision, and the "int" and "long" types are one.
        self.assertEqual(1, c / b, msg='Length / Length')
        self.assertEqual(Length(45), a / int(2), msg='Length / int')
        self.assertEqual(Length(45), a / 2.0, msg='Length / float')

        self.assertEqual(Length(90), b * 2.0, msg='Length * float')
        self.assertEqual(Length(90), 2.0 * b, msg='float * Length')
        self.assertEqual(Length(90), b * int(2), msg='Length * int')
        self.assertEqual(Length(90), int(2) * b, msg='int * Length')

        self.assertEqual(Length(20), b % e, msg='Length % Length')

        self.assertTrue(a > b, msg='greater than')
        self.assertTrue(c >= b, msg='greater than or equal')
        self.assertTrue(b == c, msg='equal')
        self.assertTrue(c < a, msg='less than')
        self.assertTrue(c <= b, msg='less than or equal')
        self.assertTrue(a != b, msg='not equal')

        self.assertEqual(Speed(90), a / timedelta(seconds=1), msg='Length / Time = Speed')
        self.assertEqual(timedelta(seconds=1), a / Speed(90), msg='Length / Speed = Time')
